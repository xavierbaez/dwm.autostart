#!/bin/sh
LOG="$HOME/Programs/logs/dwmblocks.log"
SCRIPT_NAME="dwmblocks.sh"
PID=$(ps aux | grep -i $SCRIPT_NAME | grep -v grep | awk -F ' ' '{print $2}' | head -n 1)
NUMPID=$(ps aux | grep -i $SCRIPT_NAME | grep -v grep | awk -F ' ' '{print $2}' | wc -l)
if ([ $$ -ne "$((PID))" ] && [ -n "$PID" ]) || [ "$((NUMPID))" -gt 2 ]; then
        echo  "Process already running: $PID. Number of processes: $NUMPID" | tee -a "$LOG"
        exit
fi
#dwblocks for statusbar
echo "dwmblocks script launcher... " | tee -a "$LOG"
while true; do
        if [ ! "$(pidof dwmblocks | awk -F ' ' '{ print $1}')" ]; then
                echo 'no PID starting dwmblocks' | tee -a "$LOG"
                dwmblocks 2>&1 &
        fi
        sleep 1
done
