#!/bin/sh
LOG="$HOME/Programs/logs/startdwm.log"
SCRIPT_NAME="autostart.sh"
PROGRAMS_DIR="/home/xavier/Programs"
PID=$(ps aux | grep -i $SCRIPT_NAME | grep -v grep | awk -F ' ' '{print $2}' | head -n 1)
NUMPID=$(ps aux | grep -i $SCRIPT_NAME | grep -v grep | awk -F ' ' '{print $2}' | wc -l)
if ([ $$ -ne "$((PID))" ] && [ -n "$PID" ]) || [ "$((NUMPID))" -gt 2 ]; then
	echo  "Process already running: $PID. Number of processes: $NUMPID" | tee -a "$LOG"
	exit
fi
sudo chown -cR xavier:xavier "$PROGRAMS_DIR"
echo "Starting DWM $SCRIPT_NAME $(date)" | tee "$LOG"
. "/home/xavier/Programs/model.sh"

echo "MODEL for autostart: $MODEL" | tee -a "$LOG"

echo "Starting setbg with pywal support..." | tee -a "$LOG"
sh "$HOME/Programs/setbg" >> "$LOG" 2>&1 &&
#nitrogen --restore  >> "$LOG" 2>&1 &

#Compositor for transparency
echo "Compositor for transparency... " | tee -a "$LOG"
picom --config "$HOME/.config/picom/picom.conf" >> "$LOG" 2>&1 &
#xcompmgr >> "$LOG" 2>&1 &

#asks password first login
echo "Starting gnome authentication agent..." | tee -a "$LOG"
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 >> "$LOG" 2>&1 &

#numlock
echo "Turning numlock on... " | tee -a "$LOG"
numlockx on >> "$LOG" 2>&1 &

#Autostart programs
echo "Autorstart XB Programs... " | tee -a "$LOG"

echo "Starting Trayer... " | tee -a "$LOG"
sh "$HOME/Programs/launch_trayer.sh" >> "$LOG" 2>&1 &

if [ "$MODEL" != "simulator" ]; then
    echo "Starting Dropbox Applet... " | tee -a "$LOG"
    dropbox start >> "$LOG" 2>&1 &
fi

if [ "$MODEL" != "simulator" ]; then
    echo "Starting JamesDSP... " | tee -a "$LOG"
    jamesdsp >> "$LOG" 2>&1 &
fi

echo "Starting Network Manager Applet... " | tee -a "$LOG"
nm-applet >> "$LOG" 2>&1 &

if [ "$MODEL" = "desktop" ]; then
    echo "Starting Volume Icon Applet... " | tee -a "$LOG"
    [ $(pidof -s volumeicon) ] && echo "volumeicon already loaded" | tee -a "$LOG"  || volumeicon >> "$LOG" 2>&1 &
fi

if [ "$MODEL" = "desktop" ]; then
    #clipmenu for clipboard, copyq for snippets instead of parcellite
    echo "Starting Clipboard program for snippets... " | tee -a "$LOG"
    copyq >> "$LOG" 2>&1 &
fi

#dwm status
echo "Setting dwmstatus... " | tee -a "$LOG"
dwmstatus 2>&1 > /dev/null &

#dunst notification daemon
echo "Setting dunst... " | tee -a "$LOG"
systemctl --global start dunst >> "$LOG" 2>&1 &

#Remove mouse when idle
echo "Setting unclutter... " | tee -a "$LOG"
unclutter 2>&1 > /dev/null &

if [ "$MODEL" = "simulator" ]; then
	###################
	#  ASP SIMULATOR  #
	###################
    # inotify for SGTK-bar, Sterrad and Access``
    if [ "$MODEL" = "simulator" ]; then
         echo "Launching Entr for sgtk-bar.sh... " | tee -a "$LOG"
        "$HOME/Programs/simulator/inotify.sgtk-bar.sh" 2>&1 > /dev/null &
        echo "Launching Entr for startvm.sh... " | tee -a "$LOG"
        "$HOME/Programs/simulator/inotify.startvm.sh" 2>&1 > /dev/null &
        echo "Launching Entr for start.chrome.sh... " | tee -a "$LOG"
        "$HOME/Programs/simulator/inotify.start.chrome.sh" 3>&1 > /dev/null &
    fi
    # Access, Sterrad
	notify-send --urgency=normal "ASP Access..." &
	#Google Chrome
	echo "Launching Google Chrome... " | tee -a "$LOG"
        "$HOME/Programs/simulator/chrome.disable.restore.sh"
	sleep 0.2 && google-chrome-stable --kiosk http://www.pas.com 2>&1 > /dev/null &
	#Sterrad VM
	echo "Launching Sterrad VM on tag 2... " | tee -a "$LOG"
	"$HOME/Programs/simulator/tag2.sh" && sleep 0. 2
	sleep 0.2 && "$HOME/Programs/simulator/startvm.sh" 2>&1 > /dev/null &
	echo "Flushing BI errors... " | tee -a "$LOG"
	$HOME/Programs/simulator/fix_bi__errors.sh 2>&1 > /dev/null &
    sleep 1
    simulator.xrandr.sh 2>&1 &
fi

#Right Alt
echo "Right Alt for dwm" | tee -a "$LOG"
xmodmap -e "keycode 108 = Super_L"  # reassign Alt_R to Super_L
xmodmap -e "remove mod1 = Super_L"  # make sure X keeps it out of the mod1 group
if [ "$MODEL" = "desktop" ]; then
	echo "Starting Barrier... " | tee -a "$LOG"
	sh "$HOME/Programs/launch_barriers.sh" >> "$LOG" 2>&1 &
else
	echo "Starting Barrier Headless Client... " | tee -a "$LOG"
	sh "$HOME/Programs/launch_barrierc.sh" >> "$LOG" 2>&1 &
fi

if [ "$MODEL" = "desktop" ]; then
	echo "Starting Rambox Pro... " | tee -a "$LOG"
    launch.ramboxpro.sh >> "$LOG" 2>&1 &
fi


#if [ "$MODEL" = "desktop" ]; then
#	echo "Starting Cisco AnyConnect Secure Mobility Client... " | tee -a "$LOG"
#    sh "$HOME/Programs/launch.cisco.sh" >> "$LOG" 2>&1 &
#fi

#dwblocks for statusbar
echo "Launching dwmblocks... " | tee -a "$LOG"
sh "$HOME/.dwm/dwmblocks.sh" >> "$LOG" 2>&1 &

if [ "$MODEL" = "laptop" ]; then
    echo "Launching csd-mouse... " | tee -a "$LOG"
    csd-mouse >> "$LOG" 2>&1 &
fi
